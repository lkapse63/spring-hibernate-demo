package org.lucky;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class MapKeyConverter {

	public static void main(String[] args) throws Exception {
		
		MapKeyConverter obj=new MapKeyConverter();
//		obj.mapper();
//		obj.converter();
		obj.keyChecker();
	}
	
	
	public void converter(){
		String relMapKey = "abc1-xyz1|abc2-xyz2|abc3-xyz3";
		String spaces="QuoteId|CustomerName|BankACNo|AccountType|BankIFSCCode|BankName|BranchName|BranchAddress|MICRCode|EiaAccount|EiaProvider|EiaAccountNumber|EiaIrName|HasExistingInsurance|I_ExistInsPolicyType|I_ExistInsCommencementYear|I_ExistInsSumAssured|I_ExistInsPresentStatus|II_ExistInsPolicyType|II_ExistInsCommencementYear|II_ExistInsSumAssured|II_ExistInsPresentStatus|III_ExistInsPolicyType|III_ExistInsCommencementYear|III_ExistInsSumAssured|III_ExistInsPresentStatus|IV_ExistInsPolicyType|IV_ExistInsCommencementYear|IV_ExistInsSumAssured|IV_ExistInsPresentStatus|V_ExistInsPolicyType|V_ExistInsCommencementYear|V_ExistInsSumAssured|V_ExistInsPresentStatus";

		String dbKey="java.lang.Long|java.lang.String|java.lang.String|java.lang.String|java.lang.String|java.lang.String|java.lang.String|java.lang.String|java.lang.String|java.lang.Short|java.lang.String|java.lang.String|java.lang.String|java.lang.String|java.lang.String|java.lang.Short|java.lang.Long|java.lang.Short|java.lang.String|java.lang.Short|java.lang.Long|java.lang.Short|java.lang.String|java.lang.Short|java.lang.Long|java.lang.Short|java.lang.String|java.lang.Short|java.lang.Long|java.lang.Short|java.lang.String|java.lang.Short|java.lang.Long|java.lang.Short";
		
        Map<String,Object> inMap=new HashMap<String,Object>();
        Map<String,Object> relMap=new HashMap<String,Object>();
        Map<String,Object> transMap=new HashMap<String,Object>();
        
        inMap.put("abc1", 10);
        inMap.put("abc2", 20);
//        inMap.put("abc3", 30);
        
        
        for(String rel : relMapKey.split("\\|")){
        	String[] split = rel.split("-");
        	relMap.put(split[0], split[1]);
        }
        
        Set<String> keySet = inMap.keySet();
        
        for(String key : keySet){
        	String relKey = relMap.get(key).toString();
        	transMap.put(relKey, inMap.get(key));
        }
        
//        System.out.println(transMap.toString());
        
        System.out.println("key count:: "+spaces.split("\\|").length);
        System.out.println("type count:: "+dbKey.split("\\|").length);
	}

	
	public String mapper() throws Exception{
		    Map<String,Object> inMap=new LinkedHashMap<String,Object>();
	        Map<String,Object> relMap=new LinkedHashMap<String,Object>();
	        Map<String,Object> transMap=new LinkedHashMap<String,Object>();
	        StringBuffer buffer =new StringBuffer();
	        StringBuffer buffer2 =new StringBuffer();
	        StringBuffer buffer3 =new StringBuffer();
	        StringBuffer buffer4 =new StringBuffer();
		 BufferedReader bufferedReader =new BufferedReader(new FileReader("C:/Users/FWIN01977/Desktop/IFLI/bank-details.csv"));
         String line="";
         while ((line = bufferedReader.readLine())!=null) {
        	
        	 String[] split = line.split(",");
        	 relMap.put(split[1], split[0]);
        	 buffer.append(split[1]+"-"+split[0]+"|");
        	 buffer3.append(split[1]+"|");
        	 buffer4.append(split[0]+"|");
        	 
        	 if(split[2].startsWith("VARCHAR"))
        		 buffer2.append("java.lang.String|");
        	 if(split[2].startsWith("BIGINT"))
        		 buffer2.append("java.lang.Long|");
        	 if(split[2].startsWith("BIT"))
        		 buffer2.append("java.lang.Integer|");
        	 if(split[2].startsWith("INT"))
        		 buffer2.append("java.lang.Integer|");
         }
         System.out.println(buffer);
         System.out.println(buffer2);
         System.out.println(buffer3);
         System.out.println(buffer4);
         return buffer2.toString();
	}


	public void keyChecker() throws Exception{
		String key="QuoteId|CustomerName|BankACNo|AccountType|BankIFSCCode|BankName|BranchName|BranchAddress|MICRCode|EiaAccount|EiaProvider|EiaAccountNumber|EiaIrName|HasExistingInsurance|I_ExistInsPolicyType|I_ExistInsCommencementYear|I_ExistInsSumAssured|I_ExistInsPresentStatus|II_ExistInsPolicyType|II_ExistInsCommencementYear|II_ExistInsSumAssured|II_ExistInsPresentStatus|III_ExistInsPolicyType|III_ExistInsCommencementYear|III_ExistInsSumAssured|III_ExistInsPresentStatus|IV_ExistInsPolicyType|IV_ExistInsCommencementYear|IV_ExistInsSumAssured|IV_ExistInsPresentStatus|V_ExistInsPolicyType|V_ExistInsCommencementYear|V_ExistInsSumAssured|V_ExistInsPresentStatus";
//		String type="java.lang.Long|java.lang.String|java.lang.String|java.lang.String|java.lang.String|java.lang.String|java.lang.String|java.lang.String|java.lang.String|java.lang.Short|java.lang.String|java.lang.String|java.lang.String|java.lang.String|java.lang.String|java.lang.Short|java.lang.Long|java.lang.Short|java.lang.String|java.lang.Short|java.lang.Long|java.lang.Short|java.lang.String|java.lang.Short|java.lang.Long|java.lang.Short|java.lang.String|java.lang.Short|java.lang.Long|java.lang.Short|java.lang.String|java.lang.Short|java.lang.Long|java.lang.Short";
		String type=mapper();
		String[] keyArr=key.split("\\|");
		String[] typeArr=type.split("\\|");
		
		for(int i=0; i< keyArr.length;i++){
			System.out.println(keyArr[i] +" => "+typeArr[i]);
		}
		
	}
}
