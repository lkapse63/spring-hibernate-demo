package org.lucky;

import javax.persistence.EntityManager;

import org.lucky.repository.ActorRepo;
import org.lucky.service.CartService;
import org.lucky.service.CommentService;
import org.lucky.service.OneToOneService;
import org.lucky.service.RestService;
import org.lucky.service.StockService;
import org.lucky.service.StudentService;
import org.lucky.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
public class SpringHibernateApplication implements CommandLineRunner {

	@Autowired
	ActorRepo actorRepo;

	@Autowired
	private CartService cartService;

	@Autowired private StockService stockService;

	@Autowired private UserService userService;

	@Autowired ObjectMapper objectMapper;
	
	@Autowired private RestService restService;
	
	@Autowired private CommentService commentService;

	@Value("${spring.create.user}")
	String createUser;

	@Autowired
	EntityManager entityManager;

	@Autowired
	OneToOneService oneToOneService;
	
	@Autowired StudentService studentService;


	@Override
	public void run(String... args) throws Exception {
		
		/*Object map = restService.getListObject("http://localhost:8089/comment/product/1");
		Object listMapObject = restService.get("http://localhost:8089/comment/single/1");
	
		System.out.println("arraylist object: "+map.toString());
		System.out.println("single object "+listMapObject);
		
		Map<String,Object> request=new HashMap<String, Object>();
		request.put("product", 6);
		request.put("descr", "pqr");
		
		restService.post("http://localhost:8089/comment/product", request);*/
		
		
		studentService.saveStudent();
		studentService.updateStudent(11);
		studentService.showStudent(14);
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringHibernateApplication.class, args);
	}

	@Bean
	public ObjectMapper objectMapper(){
		return new ObjectMapper();
	}
	
	@Bean
	public RestTemplate geTemplate(){
		return new RestTemplate();
	}


}
