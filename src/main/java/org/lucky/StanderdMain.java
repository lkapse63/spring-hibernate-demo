package org.lucky;

import org.lucky.annotation.AnnotationConsumer;
import org.lucky.annotation.AnnotationDemo;
import org.lucky.interview.HashKey;
import org.lucky.interview.list.CustomeList;
import org.lucky.interview.map.CustomeMap;
import org.lucky.interview.map.Key;

import ch.qos.logback.core.net.SyslogOutputStream;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StanderdMain {

    public static void main(String[] args) throws Exception {

 /*       Class<AnnotationConsumer> consumerClass = AnnotationConsumer.class;

        if(consumerClass.isAnnotationPresent(AnnotationDemo.class)){
            System.out.println("annotation present");

            Annotation[] annotations = consumerClass.getDeclaredAnnotations();
            for(Annotation annotation :annotations){
                System.out.println(annotation.annotationType());
            }

            AnnotationDemo annotationDemo = (AnnotationDemo)  consumerClass.getDeclaredAnnotation(AnnotationDemo.class);
            System.out.println("get name: "+ annotationDemo.name());
            System.out.println("get init: "+ annotationDemo.init());



        }*/

        String input="abc";
        StringBuilder builder =new StringBuilder(input);
        StringBuilder reverse = builder.reverse();
       /* System.out.println("bycode: "+rev(input));
        System.out.println("by api: "+reverse);*/

      /*  HashMap<Integer,Integer> map=new HashMap<Integer, Integer>();
        map.put(1,2);
        map.put(1,3);*/

        HashKey hashKey=new HashKey("lucky");
        HashKey hashKey2=new HashKey("lokesh");
        HashKey hashKey3=new HashKey("ravana");
       /* System.out.println(hashKey.hashCode());
        System.out.println(hashKey2.hashCode());*/
        /*HashMap<HashKey,Integer> map=new HashMap<HashKey, Integer>();
        map.put(hashKey,10);
        map.put(hashKey2,20);
        map.put(hashKey3,30);
        System.out.println("calling get method");
        System.out.print(map.get(hashKey2).toString());

        int index= hashKey3.hashCode() & 15;
        System.out.println("hash indesx => "+index);*/

       /* CustomeMap map= new CustomeMap();
        map.put(new Key("lucky"),10);
        map.put(new Key("ravan"),20);
        map.put(new Key("lokesh"),30);
        map.put(new Key("lankesh"),40);
        System.out.println("map => "+map.toString());
        System.out.println("value => "+map.get(new Key("lucky")));*/

        CustomeList customeList=new CustomeList();
        customeList.add(10);
        customeList.add(20);
        customeList.add(30);
        customeList.add(50);
        customeList.put(40,2);
//        System.out.println("list=> "+customeList.toString());
//        System.out.println("get=> "+customeList.get(2));

         String in="quoteId|customerTypeId|fatcaBirthCountry|fatcaCountryName|fatcaBirthPlace|fatcaFatherName|fatcaOtherCitizenship|fatcaTaxOtherCountry|fatcaGreenCard|fatcaUSResident|fatcaOtherResident|fatcaResidentCountry|fatcaTinNumber|fatcaIdIssuedCountry|countryOfResidency|countryOfCitizenship|telephoneOutsideIndia|fatcaAddressLine1|fatcaAddressLine2|fatcaAddressLine3|fatcaZip|fatcaCountry|fatcaState|fatcaCity|fatcaTaxResident|fatcaAttorneyPower|fatcaSourceOfIncome";
         
//         for(String param : in.split("\\|"))
//        	 System.out.print("abc,");
         
         String spaces="QuoteId,CustomerName,BankACNo,AccountType,BankIFSCCode,BankName,BranchName,BranchAddress,MICRCode,EiaAccount,EiaProvider,EiaAccountNumber,EiaIrName,HasExistingInsurance,I_ExistInsPolicyType,I_ExistInsCommencementYear,I_ExistInsSumAssured,I_ExistInsPresentStatus,II_ExistInsPolicyType,II_ExistInsCommencementYear,II_ExistInsSumAssured,II_ExistInsPresentStatus,III_ExistInsPolicyType,III_ExistInsCommencementYear,III_ExistInsSumAssured,III_ExistInsPresentStatus,IV_ExistInsPolicyType,IV_ExistInsCommencementYear,IV_ExistInsSumAssured,IV_ExistInsPresentStatus,V_ExistInsPolicyType,V_ExistInsCommencementYear,V_ExistInsSumAssured,V_ExistInsPresentStatus";
         
         for(String key:spaces.split(",")){
//        	 System.out.print((key.charAt(0)+"").toLowerCase()+key.substring(1)+",");
         }
         
         System.out.println();
         
         for(String key:spaces.split(",")){
//        	 System.out.print("java.lang.String"+"|");
         }
         
         BufferedReader bufferedReader =new BufferedReader(new FileReader("C:/Users/FWIN01977/Desktop/IFLI/inputs.txt"));
         String line="";
         StringBuffer buffer =new StringBuffer();
         StringBuffer buffer2 =new StringBuffer();
         while ((line = bufferedReader.readLine())!=null) {
        	 String substring = line.substring(0, line.lastIndexOf(":"));
			 buffer.append(substring.replaceAll("\"", "")+"|");
			 buffer2.append("java.lang.String"+"|");
			
		}
         System.out.println(buffer);
         System.out.println(buffer2);
        
    }

    public static String reverse(String input){
        char[] in = input.toCharArray();
        int begin=0;
        int end=in.length-1;
        char temp;
        while(end>begin){
            temp = in[begin];
            in[begin]=in[end];
            in[end] = temp;
            end--;
            begin++;
        }
        return new String(in);
    }

    static String rev(String input){
        char[] chars = input.toCharArray();
        int start=0;
        int end= chars.length-1;
        for(int i=0;i<chars.length/2;i++){
            char tmp=chars[start];
            chars[start]=chars[end];
            chars[end]=tmp;
            start++;
            end--;
        }
        return new String(chars);
    }
    
    
    public static Map<Object,Object> flatern(Map<String,Object> inMap){
    	
    	inMap.forEach((k,v) -> System.out.println(k));
    	return null;
    }
    
 public static void flaternList(List<Object> inList){
    	
	    inList.forEach(k-> System.out.println(k));
    }
}
