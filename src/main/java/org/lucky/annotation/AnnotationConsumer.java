package org.lucky.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@AnnotationDemo(name = "lucky",init = 2)
public class AnnotationConsumer {

    @FieldAnnotation(name = "abc")
    public String name;
}

@Target(ElementType.FIELD)
@interface FieldAnnotation{
     String name() default "";
}

@interface Generic{

}