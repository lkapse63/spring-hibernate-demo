package org.lucky.contollers;

import org.lucky.models.CommentModel;
import org.lucky.models.ProductModel;
import org.lucky.repository.ProductModelRepo;
import org.lucky.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/comment")
public class CommentController {

    @Autowired private CommentService commentService;



    @GetMapping(value = "/{productId}")
    public Object getComment(@PathVariable String productId){
        return commentService.retriveComment(productId);
    }

    @GetMapping(value = "/product/{productId}")
    public Object getProduct(@PathVariable int productId){
        return commentService.getProductComment(productId);
    }
    
    
    @GetMapping(value = "/single/{productId}")
    public Object getSingleComment(@PathVariable String productId){
        return commentService.retriveSingleComment(productId);
    }
    
    @PostMapping(value = "/product")
    public Object createProduct(@RequestBody ProductModel productModel){
        return commentService.createProduct(productModel);
    }
}
