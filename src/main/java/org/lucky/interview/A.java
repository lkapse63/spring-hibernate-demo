package org.lucky.interview;

import java.io.FileNotFoundException;

public class A {
	
	public void show() throws FileNotFoundException{
		System.out.println("from A");
	}
	
	public int add(int a, int b){
		return b;	
	}

}
