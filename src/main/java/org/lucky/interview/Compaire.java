package org.lucky.interview;

import java.util.Comparator;

public class Compaire implements Comparator<Student> {

	@Override
	public int compare(Student o1, Student o2) {
		if(o1.id > o2.id)
			return 1;
		if(o1.id < o2.id)
			return 2;
		if(o1.id == o2.id)
			return 0;
		return 0;
	}

}
