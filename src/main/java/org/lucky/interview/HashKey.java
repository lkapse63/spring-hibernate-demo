package org.lucky.interview;

import java.util.Objects;

public class HashKey {

    String key;

    public HashKey(String key) {
        this.key = key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HashKey)) return false;
        HashKey hashKey = (HashKey) o;
        System.out.println("Equals called: key=> "+hashKey+"  HashKey.key=> "+hashKey.key);
        return Objects.equals(key, hashKey.key);
    }

    @Override
    public int hashCode() {
        System.out.println("Hashcode called: "+key);
        return (int) key.charAt(0);
    }

    @Override
    public String toString() {
        return "HashKey{" +
                "key='" + key + '\'' +
                '}';
    }
}
