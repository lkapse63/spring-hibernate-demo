package org.lucky.interview;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.util.MultiValueMap;

public class Interview {

	public static void main(String[] args) {
		
		Student s1=new Student(1,"abc");
		Student s2=new Student(2,"xyz");
		
		Map<Student,Integer> map= new HashMap<>();
		map.put(s1, 10);
		map.put(s2, 20);
		System.out.println(map.get(s1));
		TreeMap<Student,Integer> treeMap = new TreeMap<Student,Integer>(new Compaire());
		
		treeMap.put(s1, 10);
		treeMap.put(s2, 20);
		
		System.out.println(treeMap.toString());
		
		
		

	}

}
