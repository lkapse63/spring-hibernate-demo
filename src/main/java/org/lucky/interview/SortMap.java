package org.lucky.interview;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;


public class SortMap {

	public static void main(String[] args) {
		HashMap<String,Integer> map= new HashMap<>();
		map.put("a", 4);
		map.put("b", 3);
		map.put("c", 2);
		map.put("d", 1);
		map.put("e", 1);
		
		Map<Integer,String> sorted= new TreeMap<>(Collections.reverseOrder());
		map.forEach((k,v) -> sorted.put(v, k));
		
		sorted.forEach((k,v) -> System.out.println(k+"=>"+v));;
	}
}
