package org.lucky.interview.list;

public class CustomeList {

    private  ListNode node;
    public void add(int value){
        if(node==null){
            node=new ListNode(value);
            return;
        }else {
            ListNode tmp=node;
            while (true){
                ListNode newNode=tmp;
                if(newNode.nextNode==null){
                    newNode.nextNode=new ListNode(value);
                    break;
                }
                tmp=newNode.nextNode;
            }
        }

    }

    public int get(int index){
        ListNode listNode=node;
        for(int i=0;i<index;i++){
            listNode=listNode.nextNode;
        }
        return listNode.value;
    }


    public void put(int value, int index){
        ListNode listNode=node;
        for(int i=0;i<index;i++){
            listNode=listNode.nextNode;
        }
        ListNode tmp=new ListNode(listNode.value,listNode.nextNode);
        listNode.value=value;
        listNode.nextNode= tmp;

    }

    @Override
    public String toString() {
        return "CustomeList{" +
                "node=" + node.toString() +
                '}';
    }
}
