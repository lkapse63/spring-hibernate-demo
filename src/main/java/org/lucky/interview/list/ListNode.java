package org.lucky.interview.list;

public class ListNode {

    public int value;
    public ListNode nextNode=null;

    public ListNode(int value) {
        this.value = value;
    }

    public ListNode(int value, ListNode nextNode){
        this.value=value;
        this.nextNode=nextNode;
    }

    @Override
    public String toString() {
        return "ListNode{" +
                "value=" + value +
                ", nextNode=" + nextNode +
                '}';
    }
}
