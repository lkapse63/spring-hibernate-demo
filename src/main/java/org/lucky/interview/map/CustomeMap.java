package org.lucky.interview.map;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CustomeMap {

    public MapNode[] map = new MapNode[16];
    private  int cap=15;

    public void put(Key key, int value){
        int hashCode = key.hashCode();
        int index = hashCode & cap;
        if (map[index] == null) {
            map[index] = new MapNode(key, value);
            return;
        } else {
            MapNode tmp=map[index];
            while (true) {
                MapNode old =tmp;
                if(old.nextNode==null){
                    old.nextNode=new MapNode(key, value);
                    break;
                }
                tmp=old.nextNode;
            }
        }
    }

    public Integer get(Key key){
        int hashCode = key.hashCode();
        int index = hashCode & cap;
        MapNode mapNodes=map[index];
        Integer value=null;
        while (true){
            if(mapNodes.key.equals(key)){
                value= mapNodes.value;
                break;
            }
            if(mapNodes.nextNode ==null)
                break;
            mapNodes=mapNodes.nextNode;
        }
        return value;
    }

    @Override
    public String toString() {
        List<MapNode> nodes = Arrays.asList(map).stream()
                .filter(mapNode -> mapNode != null)
                .collect(Collectors.toList());
        return "CustomeMap{" +
                "map=" + nodes.toString() +
                '}';
    }
}
