package org.lucky.interview.map;

import java.util.Objects;

public class Key {

    public String name;

    public Key(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Key)) return false;
        Key key = (Key) o;
        return name.equals(key.name);
    }

    @Override
    public int hashCode() {
        return (int) name.charAt(0);
    }

    @Override
    public String toString() {
        return "Key{" +
                "name='" + name + '\'' +
                '}';
    }
}
