package org.lucky.interview.map;

public class MapNode {

    public Key key;
    public Integer value;
    public MapNode nextNode=null;

    public MapNode(Key key, Integer value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString() {
        return "MapNode{" +
                "key=" + key +
                ", value=" + value +
                ", nextNode=" + nextNode +
                '}';
    }
}
