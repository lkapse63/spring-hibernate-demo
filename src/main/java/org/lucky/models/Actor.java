package org.lucky.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "actor")
public class Actor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int actorid;
    private int movieid;
    private String actorname;

    @Embedded
    @AttributeOverrides(
            value = {
                    @AttributeOverride(name = "street",column = @Column(name = "home_street")),
                    @AttributeOverride(name = "city",column = @Column(name = "home_city"))}
    )
    private Address homeAddress;

    @Embedded
    @AttributeOverrides(
            value = {
            @AttributeOverride(name = "street",column = @Column(name = "office_street")),
            @AttributeOverride(name = "city",column = @Column(name = "office_city"))}
    )
    private Address officeAddress;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "actorHobbies", joinColumns =@JoinColumn(name = "actorid"))
    private List<String> hobbies;


    public int getActorid() {
        return actorid;
    }

    public void setActorid(int actorid) {
        this.actorid = actorid;
    }

    public int getMovieid() {
        return movieid;
    }

    public void setMovieid(int movieid) {
        this.movieid = movieid;
    }

    public String getActorname() {
        return actorname;
    }

    public void setActorname(String actorname) {
        this.actorname = actorname;
    }

    public Address getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(Address homeAddress) {
        this.homeAddress = homeAddress;
    }

    public Address getOfficeAddress() {
        return officeAddress;
    }

    public void setOfficeAddress(Address officeAddress) {
        this.officeAddress = officeAddress;
    }

    public List<String> getHobbies() {
        return hobbies;
    }

    public void setHobbies(List<String> hobbies) {
        this.hobbies = hobbies;
    }

    @Override
    public String toString() {
        return "Actor{" +
                "actorid=" + actorid +
                ", movieid=" + movieid +
                ", actorname='" + actorname + '\'' +
                ", homeAddress=" + homeAddress +
                ", officeAddress=" + officeAddress +
                ", hobbies=" + hobbies +
                '}';
    }
}
