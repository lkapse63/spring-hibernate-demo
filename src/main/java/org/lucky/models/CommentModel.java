package org.lucky.models;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "commenttbl")
@NamedQueries(
        @NamedQuery(name = "getProductComments",
        query = "select comment from CommentModel comment where comment.product =:productId order by comment.ratings desc")
)
public class CommentModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "comment")
    private String comment;

    @Column(name="product")
    private int product;

    @Column(name = "ratings")
    private double ratings;

    @JsonIgnore
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @JsonIgnore
    public int getProduct() {
        return product;
    }

    public void setProduct(int product) {
        this.product = product;
    }

    public double getRatings() {
        return ratings;
    }

    public void setRatings(double ratings) {
        this.ratings = ratings;
    }

    @Override
    public String toString() {
        return "CommentModel{" +
                "id=" + id +
                ", comment='" + comment + '\'' +
                ", product='" + product + '\'' +
                ", ratings=" + ratings +
                '}';
    }
}
