package org.lucky.models.cascade;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "cart")
public class CartModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "cart_type", nullable = false)
    private int cartType;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @JoinTable(name = "cart_item",
               joinColumns = {
                       @JoinColumn(name = "cart_id")}
                       ,inverseJoinColumns = { @JoinColumn(name = "item_id")})
    private List<ItemModel> itemModel;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCartType() {
        return cartType;
    }

    public void setCartType(int cartType) {
        this.cartType = cartType;
    }

    public List<ItemModel> getItemModel() {
        return itemModel;
    }

    public void setItemModel(List<ItemModel> itemModel) {
        this.itemModel = itemModel;
    }

    @Override
    public String toString() {
        return "CartModel{" +
                "id=" + id +
                ", cartType=" + cartType +
                ", itemModel=" + itemModel +
                '}';
    }
}
