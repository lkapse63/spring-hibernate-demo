package org.lucky.models.cascade;

import javax.persistence.*;

@Entity
@Table(name = "item")
public class ItemModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int itemType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }



    @Override
    public String toString() {
        return "ItemModel{" +
                "id=" + id +
                ", itemType=" + itemType +
                '}';
    }
}
