package org.lucky.models.cascade;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "stock")
public class Stock {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "stock_id")
    private int id;

    private String name;

    @Column(name = "stock_type")
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, targetEntity = StockItem.class)
    @JoinColumn(name = "stock_id")
    private List<StockItem> stockType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<StockItem> getStockType() {
        return stockType;
    }

    public void setStockType(List<StockItem> stockType) {
        this.stockType = stockType;
    }

    @Override
    public String toString() {
        return "Stock{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", stockType=" + stockType +
                '}';
    }
}
