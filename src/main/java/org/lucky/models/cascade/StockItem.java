package org.lucky.models.cascade;

import javax.persistence.*;

@Entity
@Table(name = "stockItem")
public class StockItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "company_id")
    private int id;
    @Column(name = "company")
    private String company;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "StockItem{" +
                "id=" + id +
                ", company='" + company + '\'' +
                '}';
    }
}
