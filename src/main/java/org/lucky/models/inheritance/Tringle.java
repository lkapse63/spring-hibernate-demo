package org.lucky.models.inheritance;

import javax.persistence.*;

@Entity
//@Table(name = "tringle")
@DiscriminatorValue("1")
public class Tringle extends Shapes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String tringleName;

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String getTringleName() {
        return tringleName;
    }

    public void setTringleName(String tringleName) {
        this.tringleName = tringleName;
    }
}
