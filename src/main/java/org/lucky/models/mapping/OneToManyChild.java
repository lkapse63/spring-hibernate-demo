package org.lucky.models.mapping;

import javax.persistence.*;

@Entity
@Table(name = "onetomanychild")
public class OneToManyChild {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "child_id")
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
