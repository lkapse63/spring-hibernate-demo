package org.lucky.models.mapping;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "onetomanyparent")
public class OneToManyParent {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "parent_id")
    private int id;
    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "onetomanyjoin",
    joinColumns = {
            @JoinColumn(name = "onetomany_parent_id")
    },
    inverseJoinColumns = {
            @JoinColumn(name="onetomany_child_id")
    })
    private Collection<OneToManyChild> child= new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<OneToManyChild> getChild() {
        return child;
    }

    public void setChild(Collection<OneToManyChild> child) {
        this.child = child;
    }
}
