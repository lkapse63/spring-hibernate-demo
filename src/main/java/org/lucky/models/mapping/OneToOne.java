package org.lucky.models.mapping;

import javax.persistence.*;

@Entity
@Table(name = "onetoneparent")
public class OneToOne {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "parent_id")
    private int id;
    private String name;

    @javax.persistence.OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name = "child_id")
    private OneToOneChild oneToOneChild;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OneToOneChild getOneToOneChild() {
        return oneToOneChild;
    }

    public void setOneToOneChild(OneToOneChild oneToOneChild) {
        this.oneToOneChild = oneToOneChild;
    }

    @Override
    public String toString() {
        return "OneToOne{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", oneToOneChild=" + oneToOneChild +
                '}';
    }
}
