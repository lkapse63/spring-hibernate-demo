package org.lucky.models.mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Address")
public class StudentAddress {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "address_id")
    private int id;
	private String street;
    private String city;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Override
	public String toString() {
		return "StudentAddress [id=" + id + ", street=" + street + ", city=" + city + "]";
	}
    
    

}
