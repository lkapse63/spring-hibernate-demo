package org.lucky.repository;

import org.lucky.models.Actor;
import org.lucky.models.ProductModel;
import org.springframework.data.repository.CrudRepository;

public interface ActorRepo  extends CrudRepository<Actor,Integer> {
}
