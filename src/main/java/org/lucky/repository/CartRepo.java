package org.lucky.repository;

import org.lucky.models.cascade.CartModel;
import org.springframework.data.repository.CrudRepository;

public interface CartRepo extends CrudRepository<CartModel,Integer> {
}
