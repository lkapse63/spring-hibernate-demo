package org.lucky.repository;

import org.lucky.models.CommentModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CommentRepo extends PagingAndSortingRepository<CommentModel, Integer> {

    @Query(value = "select c.comment, c.ratings from  commenttbl c where c.product =:productId", nativeQuery = true)
    List<Object[]> findByProductId(@Param("productId") String productId);

    @Query(value = "select c from  CommentModel c where c.product =:productId order by c.ratings desc")
    List<CommentModel> findByNamedQuery(@Param("productId") String productId);
}
