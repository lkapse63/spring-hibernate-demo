package org.lucky.repository;

import org.lucky.models.ProductModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductModelRepo extends CrudRepository<ProductModel,Integer> {

    @Query("from ProductModel where product=:productId")
    List<ProductModel> getProductComment(@Param("productId") int productId);
}
