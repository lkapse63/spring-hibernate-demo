package org.lucky.repository;

import org.lucky.models.cascade.CartModel;
import org.lucky.models.cascade.Stock;
import org.springframework.data.repository.CrudRepository;

public interface StockRepo extends CrudRepository<Stock,Integer> {
}
