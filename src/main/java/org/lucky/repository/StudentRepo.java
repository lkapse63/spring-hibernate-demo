package org.lucky.repository;

import org.lucky.models.mapping.Student;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepo extends CrudRepository<Student,Integer> {

}
