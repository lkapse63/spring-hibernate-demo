package org.lucky.repository;

import org.lucky.models.cascade.Stock;
import org.lucky.models.cascade.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepo extends CrudRepository<User,Integer> {
}
