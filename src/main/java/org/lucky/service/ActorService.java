package org.lucky.service;

import org.lucky.models.Actor;
import org.lucky.models.Address;
import org.lucky.repository.ActorRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ActorService {
    @Autowired
    ActorRepo actorRepo;

    public Actor saveActor(){
        Actor actor =new Actor();

        actor.setActorname("abc");
        actor.setMovieid(1);

        Address home= new Address();
        home.setCity("ngp");
        home.setStreet("ngp");

        Address office= new Address();
        home.setCity("png");
        home.setStreet("png");

        actor.setHomeAddress(home);
        actor.setOfficeAddress(office);

        ArrayList<String> hobbies = new ArrayList<>();
        hobbies.add("reading");
        hobbies.add("playing");

        actor.setHobbies(hobbies);
        Actor save = actorRepo.save(actor);
        return save;
    }
}
