package org.lucky.service;

import org.lucky.models.cascade.CartModel;
import org.lucky.models.cascade.ItemModel;
import org.lucky.repository.CartRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CartService {

    @Autowired private CartRepo cartRepo;

    public CartModel saveCart(){
        CartModel cartModel=new CartModel();
        int cartType = (int) (Math.random() * (50 - 10)) + 10;
        cartModel.setCartType(cartType);

        List<ItemModel> itemList=new ArrayList<>();
        ItemModel itemModel;
        for(int i=1 ; i<5 ; i++){
            itemModel =new ItemModel();
            int itemType = (int) (Math.random() * (30 - 10)) + 10;
            itemModel.setItemType(itemType);
            itemList.add(itemModel);
        }
        cartModel.setItemModel(itemList);

        CartModel saved = cartRepo.save(cartModel);
//        System.out.println("saving cart model:=> "+saved.toString());
        return saved;

    }

    public void delCart(int cart){
        try {
            cartRepo.deleteById(cart);
        }catch (Exception ex){
            System.err.println(ex.getLocalizedMessage());
        }

    }

    public CartModel findById(int id){
        return cartRepo.findById(id).orElse(new CartModel());
    }
}
