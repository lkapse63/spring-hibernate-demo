package org.lucky.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.lucky.models.CommentModel;
import org.lucky.models.ProductModel;
import org.lucky.repository.CommentRepo;
import org.lucky.repository.CommentRepo2;
import org.lucky.repository.ProductModelRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CommentService {

//    @Autowired private SessionFactory sessionFactory;

    @Autowired private EntityManager entityManager;

    @Autowired private CommentRepo2 commentRepo;

    @Autowired private ProductModelRepo productModelRepo;


    public Object retriveComment(String productId){
        Session session = entityManager.unwrap(Session.class);
       /* return session
                .createNativeQuery("select * from commenttbl  where product="+productId,CommentModel.class)
                .getResultList();*/
        ArrayList<Integer> ids = new ArrayList<>();
        ids.add(Integer.parseInt(productId));
        Iterable<CommentModel> allById = commentRepo.findAllById(ids);
        return allById;
//        return commentRepo.findByNamedQuery(productId);

        /*return session.createNamedQuery("getProductComments",CommentModel.class)
                .setParameter("productId",productId)
                .getResultList();*/
    }


    public Object getProductComment(int productId){
        return productModelRepo.getProductComment(productId);
    }
    
    @Transactional
    public Object retriveSingleComment(String productId){
        Session session = entityManager.unwrap(Session.class);
       return session
                .createNativeQuery("select * from producttbl  where product="+productId,ProductModel.class)
                .getSingleResult();
        
    }


    @Transactional
	public Object createProduct(ProductModel productModel) {
		ProductModel save = productModelRepo.save(productModel);
		return save;
	}
}
