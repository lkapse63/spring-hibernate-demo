package org.lucky.service;

import org.hibernate.Session;
import org.lucky.models.mapping.OneToOne;
import org.lucky.models.mapping.OneToOneChild;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;

@Service
public class OneToOneService {

    @Autowired
    EntityManager entityManager;

    @Transactional
    public void save(){
        OneToOne oneToOne=new OneToOne();
        oneToOne.setName("parent");
        OneToOneChild oneToOneChild=new OneToOneChild();
        oneToOneChild.setName("child");
        oneToOne.setOneToOneChild(oneToOneChild);
        Session session = entityManager.unwrap(Session.class);
        session.persist(oneToOneChild);
        session.persist(oneToOne);
//        session.flush();
    }

    public OneToOne get(){
        Query nativeQuery = entityManager.createNativeQuery("select * from onetoneparent", OneToOne.class);
        return (OneToOne) nativeQuery.getResultList().get(0);
    }
}
