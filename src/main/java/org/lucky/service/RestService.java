package org.lucky.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class RestService {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired ObjectMapper objectMapper;
	
	public Object getListObject(String serviceURL) throws JsonParseException, JsonMappingException, IOException {
		 List<HashMap<String,Object>> forObject = restTemplate.getForObject(serviceURL, List.class);
		 return forObject;
	}
	
	public Object get(String serviceURL) throws JsonParseException, JsonMappingException, IOException {
		 Map<String,Object> forObject = restTemplate.getForObject(serviceURL, LinkedHashMap.class);
		 return forObject;
	}
	
	public Object post(String serviceURL, Map<String,Object> requestBody){
		ResponseEntity<Object> postForEntity = restTemplate.postForEntity(serviceURL, requestBody, Object.class);
		return postForEntity;
		
	}
 
}
