package org.lucky.service;

import org.lucky.models.cascade.Stock;
import org.lucky.models.cascade.StockItem;
import org.lucky.repository.StockRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StockService {

    @Autowired
    private StockRepo stockRepo;

    public Stock saveStock(){
        Stock stock =new Stock();

        StockItem stockItem;
        List<StockItem> stockItems= new ArrayList<>();
        for(int i=0;i<3;i++){
            stockItem=new StockItem();
            stockItem.setCompany(""+(int)(Math.random()*100)+"xyz");
            stockItems.add(stockItem);
        }

        stock.setStockType(stockItems);
        stock.setName("abc"+(int)(Math.random()*100));
        Stock save = stockRepo.save(stock);

        System.out.println("stock saved: "+stock.toString());
        return save;
    }

    public Stock findById(int id){
       return stockRepo.findById(id).orElse(new Stock());
    }

    public void delStock(int id){
        stockRepo.deleteById(id);
    }
}
