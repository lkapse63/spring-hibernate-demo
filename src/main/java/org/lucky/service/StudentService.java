package org.lucky.service;

import java.util.Optional;

import org.lucky.models.mapping.Student;
import org.lucky.models.mapping.StudentAddress;
import org.lucky.repository.StudentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

	@Autowired
	private StudentRepo studentRepo;
	
	public void saveStudent(){
		
		Student student=new Student();
		student.setName("abc");
		
		StudentAddress stAddr1=new StudentAddress();
		stAddr1.setCity("pune");
		stAddr1.setStreet("mg road1");
		

		StudentAddress stAddr2=new StudentAddress();
		stAddr2.setCity("pune2");
		stAddr2.setStreet("mg road2");
		
		student.getAddress().add(stAddr1);
		student.getAddress().add(stAddr2);
		
		studentRepo.save(student);
	}

    public void updateStudent(int id){
    	
    	Optional<Student> studentOptional = studentRepo.findById(id);
    	Student student = studentOptional.get();
    	
    	StudentAddress stAddr1=new StudentAddress();
		stAddr1.setCity("pune");
		stAddr1.setStreet("mg road1");
		student.getAddress().clear();
		student.getAddress().add(stAddr1);
		studentRepo.save(student);
    	
    }
    
    public void showStudent(int id){
    	Optional<Student> studentOptional = studentRepo.findById(id);
    	Student student = studentOptional.get();
    	System.out.println("Student:: \n"+student.toString());
    }
}
