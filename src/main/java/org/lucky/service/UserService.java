package org.lucky.service;

import org.lucky.models.cascade.Phone;
import org.lucky.models.cascade.User;
import org.lucky.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired private UserRepo userRepo;

    @Autowired private EntityManager entityManager;

    public User saveUser(){

        User user=new User();

        user.setUsername("lucky");

        Phone phone;
        List<Phone> phones=new ArrayList<>();
        for(int i=0;i<3;i++){
            phone=new Phone();
            phone.setPhoneName("iphone");
            phone.setUser(user);
            phones.add(phone);
        }

        user.setPhones(phones);

        return userRepo.save(user);
    }

    public User saveUser(User user){
        return userRepo.save(user);
    }

    @Transactional
    public User getUser(int id){
        User user=new User();
        TypedQuery<User> query = entityManager.createNamedQuery("get.user", User.class)
                .setParameter("id", id);
        user = query.getSingleResult();
        return user;
    }
}
